import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusComponent } from './status/status.component';
import { LabelTagComponent } from './label-tag/label-tag.component';
import { PriorityComponent } from './priority/priority.component';
import { CountryComponent } from './country/country.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        StatusComponent,
        LabelTagComponent,
        PriorityComponent,
        CountryComponent],
    exports: [
        StatusComponent,
        LabelTagComponent,
        PriorityComponent,
        CountryComponent

    ]
})
export class SharedModule { }