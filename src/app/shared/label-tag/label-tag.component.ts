import { Component, Input, OnInit } from '@angular/core';

export interface LabelTagStyle {
  color: string;
  backgroundColor: string;
}

@Component({
  selector: 'app-label-tag',
  templateUrl: './label-tag.component.html',
  styleUrls: ['./label-tag.component.scss']
})

export class LabelTagComponent implements OnInit {
  @Input() text: string;
  @Input() style: LabelTagStyle;

  constructor() { }

  ngOnInit(): void {
  }

}
