import { Component, Input, OnInit } from '@angular/core';
import { LabelTagStyle } from '../label-tag/label-tag.component';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})

export class StatusComponent implements OnInit {
  STYLE_DEFINITION: { [key: string]: LabelTagStyle} = {
    ['Open']: {
      color: '#1268D3',
      backgroundColor: '#E5EDFA'
    },
    ['Closed']: {
      color: '#a5c34d',
      backgroundColor: '#e5f1e4'
    }
  }

  @Input() status: string;
  constructor() { }

  ngOnInit(): void {
  }

  get styleDefinition() {
    return this.STYLE_DEFINITION[this.status];
  }

}
