import { Component, Input, OnInit } from '@angular/core';
import { LabelTagStyle } from '../label-tag/label-tag.component';

@Component({
  selector: 'app-priority',
  templateUrl: './priority.component.html',
  styleUrls: ['./priority.component.scss']
})
export class PriorityComponent implements OnInit {
  STYLE_DEFINITION: { [key: string]: LabelTagStyle} = {
    ['Low']: {
      color: '#788594',
      backgroundColor: '#f9fafb'
    },
    ['Medium']: {
      color: '#e0b30e',
      backgroundColor: '#fff8a4e0'
    },
    ['High']: {
      color: '#de3636',
      backgroundColor: '#fbe5e5'
    },
    ['Critical']: {
      color: '#fbfcfc',
      backgroundColor: '#c4cfd6'
    },
  }

  @Input() priority: string;
  
  constructor() { }

  ngOnInit(): void {
  }

  get styleDefinition() {
    return this.STYLE_DEFINITION[this.priority];
  }

}
