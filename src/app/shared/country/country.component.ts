import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})


export class CountryComponent implements OnInit {
  COUNTRIES_IMAGES_PATH = './assets/countries/'
  COUNTRY_IMAGES_DIC = {
    ['il'] : `${this.COUNTRIES_IMAGES_PATH}israel.jpg`,
    ['us']: `${this.COUNTRIES_IMAGES_PATH}us.jpg`
  }
  
  @Input() country: string;

  imgSrc: string;

  constructor() { }

  ngOnInit(): void {
    this.imgSrc = this.COUNTRY_IMAGES_DIC[this.country];
  }

}
