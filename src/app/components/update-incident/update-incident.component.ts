import { Component, Input, OnInit } from '@angular/core';
import { IncidentService } from 'src/app/servcies/incident.service';
import { EventType, Incident } from 'src/app/types';
import { STATUSES, PRIORITIES, COUNTIRES } from '../../utils';
@Component({
  selector: 'app-update-incident',
  templateUrl: './update-incident.component.html',
  styleUrls: ['./update-incident.component.scss']
})
export class UpdateIncidentComponent implements OnInit {
  @Input() incident: Incident;

  statuses = STATUSES;
  priorities = PRIORITIES;
  countries = COUNTIRES;

  constructor(private incidentService: IncidentService) { }

  ngOnInit(): void {
  }

  updateIncident(propName: EventType, newVal: string) {
    const oldVal = this.incident[propName];
    this.incident = { ...this.incident, [propName]: newVal };
    this.incidentService.update(this.incident._id, { propName, oldVal, newVal })
      .subscribe(() => { });
  }
}
