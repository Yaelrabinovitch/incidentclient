import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-select-items',
  templateUrl: './select-items.component.html',
  styleUrls: ['./select-items.component.scss']
})
export class SelectItemsComponent implements OnInit {
  @Input() items: string[];
  @Input() selectedItem: string;
  @Output() itemSelected = new EventEmitter<string>();
  @ContentChild('optionTemplate', { static: false }) optionTemplate: TemplateRef<any>;
  
  constructor() { }

  ngOnInit(): void { }

  onChange(newValue: string) {
    this.itemSelected.emit(newValue);
  }

}
