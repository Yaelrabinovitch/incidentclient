import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { IncidentService } from 'src/app/servcies/incident.service';
import { IncidentEvent } from 'src/app/types';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  constructor(private incidentService: IncidentService) { }

  @Input() incidentId: string;

  incidentEvents$: Observable<IncidentEvent[]>;

  ngOnInit(): void {
    this.incidentEvents$ = this.incidentService.updateIncidentEvent$
      .pipe(filter(id => !id || id === this.incidentId),
        switchMap(() =>
          this.incidentService.getEvents(this.incidentId)
        ),
        map(this.sortEventsByDate));
  }

  sortEventsByDate(incidentEvents: IncidentEvent[]) {
    return incidentEvents.sort((eventA, eventB) => new Date(eventB.timestamp).getTime() - new Date(eventA.timestamp).getTime());
  }

  trackByEvent(index, event: IncidentEvent) {
    return event._id;
  }

}
