import { Component, Input, OnInit } from '@angular/core';
import { IncidentEvent } from 'src/app/types';

@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.scss']
})
export class SingleEventComponent implements OnInit {
  @Input() event: IncidentEvent;
  constructor() { }

  ngOnInit(): void {
  }

}
