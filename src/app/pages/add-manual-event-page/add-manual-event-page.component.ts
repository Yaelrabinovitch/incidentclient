import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IncidentService } from 'src/app/servcies/incident.service';

@Component({
  selector: 'app-add-manual-event-page',
  templateUrl: './add-manual-event-page.component.html',
  styleUrls: ['./add-manual-event-page.component.scss']
})
export class AddManualEventPageComponent implements OnInit {
  
  manualEventForm = new FormGroup({
    name: new FormControl('', [
      Validators.required]),
    description: new FormControl(''),
  });

  getFormControlValue = (formControlName) => this.manualEventForm.get(formControlName).value;
  incidentId: string;

  constructor(private incidentService: IncidentService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.incidentId = this.route.snapshot.params.incidentId;
  }

  navigateToIncidentPage() {
    this.router.navigateByUrl(`/incident/${this.incidentId}`);
  }

  cancel() {
    this.navigateToIncidentPage();
  }

  addEvent() {
    const nameValue = this.getFormControlValue('name');
    const descriptionValue = this.getFormControlValue('description')
    this.incidentService.addManualEvent(this.incidentId, { name: nameValue, description: descriptionValue }).subscribe(() => {
      this.navigateToIncidentPage();
    });
  }

}
