import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddManualEventPageComponent } from './add-manual-event-page.component';

describe('AddManualEventPageComponent', () => {
  let component: AddManualEventPageComponent;
  let fixture: ComponentFixture<AddManualEventPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddManualEventPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddManualEventPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
