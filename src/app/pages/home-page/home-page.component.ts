import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IncidentService } from 'src/app/servcies/incident.service';
import { Incident } from 'src/app/types';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  incidents$: Observable<Incident>;

  constructor(private incidentService: IncidentService) { }

  ngOnInit(): void {
    this.incidents$ = this.incidentService.getAll();
  }

}
