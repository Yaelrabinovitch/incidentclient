import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IncidentService } from 'src/app/servcies/incident.service';
import { Incident } from 'src/app/types';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-incident-page',
  templateUrl: './incident-page.component.html',
  styleUrls: ['./incident-page.component.scss']
})
export class IncidentPageComponent implements OnInit {
  incident$: Observable<Incident>;
  incidentId$: Observable<string>;

  constructor(private route: ActivatedRoute,
              private incidentService: IncidentService) { }

  ngOnInit(): void {
    this.incidentId$ = this.route.paramMap.pipe(map(params => params.get('id')));
    this.incident$ = this.incidentId$.pipe(switchMap(incidentId => {
      return this.incidentService.getById(incidentId);
    }))
  }
}
