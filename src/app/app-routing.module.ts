import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AddManualEventPageComponent } from './pages/add-manual-event-page/add-manual-event-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { IncidentPageComponent } from './pages/incident-page/incident-page.component';

const routes: Routes =
  [{ path: 'home', component: HomePageComponent },
  { path: 'incident/:id', component: IncidentPageComponent },
  { path: 'addManualEvent/:incidentId', component: AddManualEventPageComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
