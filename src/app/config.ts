const SERVER_URL = 'http://localhost:3000/api/';
const INCIDENT_ROUTE = `${SERVER_URL}incident/`
const EVENT_ROUTE =  `${SERVER_URL}event/`

export { SERVER_URL, INCIDENT_ROUTE, EVENT_ROUTE };