import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IncidentPageComponent } from './pages/incident-page/incident-page.component';
import { SelectItemsComponent } from './components/update-incident/select-items/select-items.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { UpdateIncidentComponent } from './components/update-incident/update-incident.component';
import { AddManualEventPageComponent } from './pages/add-manual-event-page/add-manual-event-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SingleEventComponent } from './components/timeline/single-event/single-event.component';
@NgModule({
  declarations: [
    AppComponent,
    SelectItemsComponent,
    IncidentPageComponent,
    TimelineComponent,
    UpdateIncidentComponent,
    AddManualEventPageComponent,
    HomePageComponent,
    SingleEventComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    NgSelectModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
