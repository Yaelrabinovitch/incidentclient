export type EventType = "status" | "priority" | "country" | "manual";

export interface IncidentEvent {
    _id: string;
    timestamp: number;
    incidentId: string;
    eventType: EventType;
    newVal: string;
    oldVal: string;
    desciprtion?: string;
}

export interface Incident {
    _id: string;
    status: string;
    priority: string;
    country: string;
}