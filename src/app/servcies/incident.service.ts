import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { EVENT_ROUTE, INCIDENT_ROUTE } from '../config';
import { EventType, Incident, IncidentEvent } from '../types';

type incidentId = string;

@Injectable({
  providedIn: 'root'
})
export class IncidentService {

  constructor(private http: HttpClient) { }

  /** Triggered every time update of the incident happens. 
   * The emitted value is the ID of the updated incident */
  private _updateIncidentEvent$: BehaviorSubject<incidentId> = new BehaviorSubject<incidentId>(null);
  public readonly updateIncidentEvent$ = this._updateIncidentEvent$.asObservable();

  getById(id: string) {
    return this.http.get<Incident>(`${INCIDENT_ROUTE}${id}`);
  }

  getAll() {
    return this.http.get<Incident>(`${INCIDENT_ROUTE}`);
  }

  update(id: string, updateData: { propName: EventType, oldVal: string, newVal: string }) {
    const updateRequest$ = this.http.post(`${INCIDENT_ROUTE}update/${id}`, updateData).pipe(shareReplay(1));
    updateRequest$.subscribe(() => {
      this._updateIncidentEvent$.next(id);
    })

    return updateRequest$;
  }

  getEvents(id: string) {
    return this.http.get<IncidentEvent[]>(`${INCIDENT_ROUTE}events/${id}`);
  }

  addManualEvent(incidentId: string,
    event: {
      description: string,
      name: string
    }) {
    return this.http.post<any>(`${EVENT_ROUTE}addManual/${incidentId}`, event);
  }

}
